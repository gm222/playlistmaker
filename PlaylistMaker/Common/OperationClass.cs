﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.UI.Xaml.Media.Imaging;

namespace PlaylistMaker.Common
{
    public interface IContent{ }

    class Directory : IContent
    {
        private StorageFolder _folder;
        
        public String           Path    { get { return this._folder.Path;      } }
        public String           Name    { get { return this._folder.Name;      } }
        public  StorageFolder   Folder  { get { return this._folder;    } set { this._folder = value;   } }


    }

    class File : IContent, INotifyPropertyChanged
    {
        private StorageFile _plik;
        private BitmapImage _cover  = new BitmapImage();
        private bool vis    = false;
        #region Stare
        //public String       Name    { get { return this._name;  } set { this._name = value; } }
        // public String      Path    { get { return this._path;  } set { this._path = value; } }
        #endregion

        public StorageFile  Plik    { get { return this._plik;  } set { this._plik = value; } }
        public String       Path    { get { return this._plik.Path;                         } }
        public String       Name    { get { return this._plik.Name;                         } }
        public String       Title   { get { return music.Title; }                             }
        public String       Album   { get { return music.Album; }                             }
        public String       Artist  { get { return music.Artist;}                             }
        public BitmapImage  Cover   { get { return this._cover; }                             }
        public Boolean      Vis     { get { return vis; } set { vis = value; OnPropertyChanged(); } }
        public MusicProperties music;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged([CallerMemberName]string propertyName = null)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        public File(StorageFile it)
        {
            this.Plik = it;
            #region Pomysł
            /*
            Task.Run<bool>(() => //FUNKCJA maa zwrócić bool czy została anulowana
            {
                return Pobierz();
            });
             */
            #endregion
        }

        public File(MusicProperties _muz, StorageFile _file, BitmapImage _cov)
        {
            this.music      =   _muz;
            this._plik      =   _file;
            this._cover     =   _cov;
        }

        public async Task<bool> Pobierz()
        {
            music = await _plik.Properties.GetMusicPropertiesAsync();
            var thumbnail = await _plik.GetThumbnailAsync(ThumbnailMode.MusicView, 50, ThumbnailOptions.UseCurrentScale);
            _cover.SetSource(thumbnail);

            return true;
        }
    }

    class Naglowek
    {//tutaj dorobic do visibility
        private String _path;
        private String _name;
        private bool _isvisable;

        public String Path { get { return this._path; } set { this._path = value; } }
        public String Name { get { return this._name; } set { this._name = value; } }
        public Boolean IsVisable { get { return this._isvisable; } private set { this._isvisable = value; } }
        public StorageFolder Current;
        public Naglowek Prev;

        public Naglowek(StorageFolder root, StorageFolder current)
        {
            if (root == null)
            {
                Name = "Path";
                _isvisable = false;
                return;
            }

            Current = current;
            Path = current.Path;
            Name = current.Name;
            
            if (root.Path != current.Path)
                _isvisable = true;
            else
                _isvisable = false;
        }

        public String Poprzedni()
        {
            return Path.Substring(0, Path.Length - Name.Length);
        }
    }

    class Domyslny
    {
        private BitmapImage _cover = new BitmapImage(new Uri("ms-appx:/Assets/music.png", UriKind.Absolute));
        public String Title { get { return "Title"; } }
        public String Artist { get { return "Artist"; } }
        public BitmapImage Cover { get { return this._cover; } }
    }
}
