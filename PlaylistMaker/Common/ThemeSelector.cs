﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace PlaylistMaker.Common
{
    class ThemeSelector : DataTemplateSelector
    {
        public DataTemplate klasa1 { get; set; }
        public DataTemplate klasa2 { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var objekt  = item as IContent;
            var dt      = objekt is Directory ? this.klasa1 : this.klasa2;

            return dt;

        }
    }
}
