﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Storage.Streams;
using Windows.UI.Xaml.Media.Imaging;


    class Schemat
    {
        private String _text;
        private BitmapImage _img = new BitmapImage();

        public String Text { get { return _text; } set { _text = value; } }
        public BitmapImage Img { get { return _img; } }

        public Schemat(String text, IRandomAccessStream stream)
        {
            _text = text;
            _img.SetSource(stream);
        }
        
    }

