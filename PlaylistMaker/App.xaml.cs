﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System;
using Windows.UI.ApplicationSettings;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace PlaylistMaker
{
    sealed partial class App : Application
    {
        public static bool _dostep;

        public App()
        {
            this.InitializeComponent();
            this.Suspending += OnSuspending;
            _dostep = false;
            //DebugSettings.EnableFrameRateCounter = true;
        }
        Frame rootFrame = Window.Current.Content as Frame;
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            

            if (rootFrame == null)
            {
                rootFrame = new Frame();

                if (args.PreviousExecutionState == ApplicationExecutionState.Terminated){}

                //Open();
                SettingsPane.GetForCurrentView().CommandsRequested += OnCommandsRequested;
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
                if (!rootFrame.Navigate(typeof(MainPage), args.Arguments))
                    throw new Exception("Failed to create initial page");
            
            Window.Current.Activate();
        }
       
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            //TODO: Save application state and stop any background activity
            deferral.Complete();
        }


        private void OnCommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            //Zmiana dostępu do plików
            var pliki_dostep = new SettingsCommand("pliki", "File and Folder Access", (handler) =>
            {
                var settings = new Callisto.Controls.SettingsFlyout()
                {
                    ContentBackgroundBrush  = new SolidColorBrush(Windows.UI.Color.FromArgb(255, 0, 0, 0)),
                    Content                 = new SettingsBar.Dostep(),
                    HeaderText              = "Files/Folders",
                    IsOpen                  = true
                };
            });

            var privacy = new SettingsCommand("privacypolicy", "Privacy Policy", OpenPrivacyPolicy);
            var ocen    = new SettingsCommand("ocenkomentarz", "Rate and review", OpenReviewComment);
            var tut    = new SettingsCommand("tutek", "Tutorial", Tutorial);

            args.Request.ApplicationCommands.Add(pliki_dostep);
            args.Request.ApplicationCommands.Add(privacy);
            args.Request.ApplicationCommands.Add(ocen);
            args.Request.ApplicationCommands.Add(tut);

        }

        private void Tutorial(IUICommand command)
        {
            rootFrame.Navigate(typeof(StartPage));
        }

        private async void OpenReviewComment(IUICommand command)
        {
            await Launcher.LaunchUriAsync(new Uri("ms-windows-store:REVIEW?PFN=5623gm222.PlayListMaker_pg6tpmnpf1jnm"));
        }

        private async void OpenPrivacyPolicy(IUICommand command)
        {
            var uri = new Uri("http://student.math.uni.opole.pl/~id11130/PolitykaPrywatnosci/PolitykaPrywatności1.pdf");
            await Launcher.LaunchUriAsync(uri);
        }


        
    }
}
