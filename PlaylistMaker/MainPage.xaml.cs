﻿using PlaylistMaker.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media.Playlists;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace PlaylistMaker
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class MainPage : PlaylistMaker.Common.LayoutAwarePage
    {
        bool drugiraz = false;
        List<IContent> lokalizacja  = new List<IContent>();
        DispatcherTimer timer       = new DispatcherTimer();
        bool _sliderpressed         = false;
        Domyslny domy = new Domyslny();
        private DispatcherTimer _timer;
        StorageFolder root;

        public MainPage()
        {
            this.InitializeComponent();
            _timer = new DispatcherTimer();
            timer.Tick      +=  new EventHandler<object>(Ticc);
            timer.Interval  =   new TimeSpan(0, 0, 0, 3, 0);

            slider.ValueChanged         += slider_ValueChanged;
            var pointerpressedhandler   = new PointerEventHandler(slider_PointerEntered);
            var pointerreleasedhandler  = new PointerEventHandler(slider_PointerCaptureLost);

            slider.AddHandler(Control.PointerPressedEvent, pointerpressedhandler, true);
            slider.AddHandler(Control.PointerCaptureLostEvent, pointerreleasedhandler, true);

            media.MediaOpened   += media_MediaOpened;
            media.MediaEnded    += media_MediaEnded;

            LOK.DataContext =   null;
            LOK.DataContext =   new Naglowek(null, null);

            PlayerInfo.DataContext = null;
            PlayerInfo.DataContext = domy;

        }

        #region Slider i MediaElement
        private void slider_ValueChanged(object sender, RangeBaseValueChangedEventArgs e)
        {
            if (!_sliderpressed)
                media.Position = TimeSpan.FromSeconds(e.NewValue);
        }

        private void SetupTimer()
        {
            
            //_timer.Interval = TimeSpan.FromSeconds(slider.StepFrequency);
            StartTimer();
        }

        private void _timer_Tick(object sender, object e)
        {
            if (!_sliderpressed)
            {
                //CurentTime.Text = String.Format("{0:hh\\:mm\\:ss}", media.Position);
                slider.Value = media.Position.TotalSeconds;
            }
        }

        private void StartTimer()
        {
            _timer.Tick += _timer_Tick;
            _timer.Start();
        }

        private void StopTimer()
        {
            _timer.Stop();
            _timer.Tick -= _timer_Tick;
        }

        private void media_MediaOpened(object sender, RoutedEventArgs e)
        {
            //AllTime.Text = String.Format("{0:hh\\:mm\\:ss}", media.NaturalDuration.TimeSpan);
            double absvalue = media.NaturalDuration.TimeSpan.TotalSeconds;

            slider.Maximum = absvalue;

            //slider.StepFrequency = SliderFrequency(media.NaturalDuration.TimeSpan);

            SetupTimer();
        }

        void slider_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            _sliderpressed = true;
        }

        void slider_PointerCaptureLost(object sender, PointerRoutedEventArgs e)
        {
            media.Position = TimeSpan.FromSeconds(slider.Value);
            //CurentTime.Text = String.Format("{0:hh\\:mm\\:ss}", media.Position);
            _sliderpressed = false;
        }

        void media_CurrentStateChanged(object sender, RoutedEventArgs e)
        {
            if (media.CurrentState == MediaElementState.Playing)
            {
                if (_sliderpressed)
                    _timer.Stop();
                else
                    _timer.Start();
            }

            if (media.CurrentState == MediaElementState.Paused)
                _timer.Stop();

            if (media.CurrentState == MediaElementState.Stopped)
            {
                _timer.Stop();
                slider.Value = 0;
            }
        }

        void media_MediaEnded(object sender, RoutedEventArgs e)
        {
            StopTimer();
            slider.Value = 0.0;
            button1.Style = Application.Current.Resources["PlayButtonStyle"] as Style;
        }

        private void PlayPause(object sender, RoutedEventArgs e)
        {
            if (media.CurrentState == MediaElementState.Closed)
                return;

            if (media.CurrentState == MediaElementState.Playing)
            {
                StopTimer();
                media.Pause();
                (sender as Button).Style = Application.Current.Resources["PlayButtonStyle"] as Style;
            }
            else
            {
                SetupTimer();
                media.Play();
                (sender as Button).Style = Application.Current.Resources["PauseButtonStyle"] as Style;
            }
        }

        private void Stop(object sender, RoutedEventArgs e)
        {
            media.Stop();
            //ply.Style = Application.Current.Resources["PlayAppBarButtonStyle"] as Style;
        }

        public async Task<bool> ReadAudioFile(StorageFile file)
        {
            //ply.Style = Application.Current.Resources["PlayAppBarButtonStyle"] as Style;
            try
            {
                var stream = await file.OpenAsync(FileAccessMode.Read);
                media.SetSource(stream, file.ContentType);
            }
            catch(Exception)
            {
                return false;
            }
            return true;
        } 

        #endregion

        #region Oryginalne funkcje
        protected override void LoadState(Object navigationParameter, Dictionary<String, Object> pageState){}
            protected override void SaveState(Dictionary<String, Object> pageState){}
        #endregion

        private void ListView_DragItemsStarting(object sender, DragItemsStartingEventArgs e)
        {
            foreach (var item in e.Items)
                e.Data.Properties.Add("Klasa", item);     
        }

        private async void OpenLok(object sender, RoutedEventArgs e)
        {
            if (App._dostep != true)
            {
                await (new Windows.UI.Popups.MessageDialog("Enable access to files and folders.", "Information")).ShowAsync();
                return;
            }

            FolderPicker folderpik = new FolderPicker();
            folderpik.ViewMode = PickerViewMode.Thumbnail;
                folderpik.FileTypeFilter.Add(".mp3");
                folderpik.FileTypeFilter.Add(".wav");
                folderpik.FileTypeFilter.Add(".wma");
                folderpik.FileTypeFilter.Add(".aac");
            folderpik.SuggestedStartLocation = PickerLocationId.ComputerFolder;

            var f = await folderpik.PickSingleFolderAsync();

            if (f != null)
            {
                lokalizacja.Clear();
                root = f;
                var objekt = await root.GetItemsAsync();

                lokalizacja = await GenerujLok(objekt);
                LOK.DataContext = null;
                var nag = new Naglowek(f, f);
                nag.Prev = nag;
                LOK.DataContext = nag;
                _lok.ItemsSource = lokalizacja;
            }//if

        }

        private async Task<List<IContent>> GenerujLok(IReadOnlyCollection<IStorageItem> objekt)
        {
            List<string> rozsz = new List<string>()
            {
                ".mp3",
                ".wma",
                ".wav",
                ".aac"
            };

            List<IContent> lokf = new List<IContent>();

            foreach (var item in objekt)
            {
                if (item is StorageFolder)
                {
                    var it = item as StorageFolder;
                    lokf.Add(new Directory() { Folder = it });
                }
                else
                {
                    var it = item as StorageFile;
                    if (rozsz.Contains(it.FileType))
                    {
                        var pl = new File(it);
                        await pl.Pobierz();
                        lokf.Add(pl);
                    }
                }
            }//foreach

            return lokf;
        }

        private async Task<bool> PoberzMuzyke(StorageFolder cur, List<File> tab, List<string> roz)
        {
            var f = await cur.GetItemsAsync();

            foreach (var objekt in f)
            {
                if (objekt is StorageFolder)
                    await PoberzMuzyke(objekt as StorageFolder, tab, roz);
                else
                {
                    var it = objekt as StorageFile;

                    if (roz.Contains(it.FileType))
                    {
                        var fi = new File(it);
                        await fi.Pobierz();

                        tab.Add(fi);
                    }
                }
            }

            return true;
        }

        private async void CanGoBack(object sender, RoutedEventArgs e)
        {
            if (App._dostep != true)
            {
                await (new Windows.UI.Popups.MessageDialog("Enable access to files and folders.", "Information")).ShowAsync();
                return;
            }

            var obj = LOK.DataContext as Naglowek;
            var fold = obj.Prev.Current;
            var zbior = await fold.GetItemsAsync();

            lokalizacja = await GenerujLok(zbior);

            var nag = obj.Prev;
            nag.Prev = (LOK.DataContext as Naglowek).Prev.Prev;

            LOK.DataContext = nag;
            _lok.ItemsSource = lokalizacja;
            
        }

        private async void ListView_Drop(object sender, DragEventArgs e)
        {

            if (App._dostep != true)
            {
                await (new Windows.UI.Popups.MessageDialog("Enable access to files and folders.", "Information")).ShowAsync();
                return;
            }

            DataPackageView dpView = e.Data.GetView();
            List<string> rozsz = new List<string>()
            {
                ".mp3",
                ".wma",
                ".wav",
                ".aac"
            };

            foreach (var prop in dpView.Properties)
            {
                var g = prop.Value as IContent;

                if (g is Directory)
                {
                    var l = new List<File>();

                    await PoberzMuzyke((g as Directory).Folder, l, rozsz);

                    foreach (var ele in l)
                    {
                        var ite = new File(ele.music, ele.Plik, ele.Cover);
                        _playlista.Items.Add(ite);
                    }
                }
                else
                {
                    var ele = g as File;
                    var ite = new File(ele.music, ele.Plik, ele.Cover);
                    _playlista.Items.Add(ite);
                }
            }
        }

        private void SzerokoscInfo_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (_playlista != null && _playlista.Items.Count != 0)
            {
                
                for (int i=0; i<_playlista.Items.Count;i++)
                {
                    StackPanel asd = new StackPanel();
                    ListViewItem lvi = (ListViewItem)_playlista.ItemContainerGenerator.ContainerFromIndex(i);
                        
                    if (lvi != null)
                        asd = FindByName("PlaylistStackPanel", lvi) as StackPanel;
                    else
                        asd = null;
                        
                    if (asd != null)
                        asd.Width = SzerokoscInfo.ActualWidth;
                }
                
                timer.Start();
            }
        }


        private void Ticc(object sender, object e)
        {
            for (int i = 0; i < _playlista.Items.Count; i++)
            {
                StackPanel asd = new StackPanel();
                ListViewItem lvi = (ListViewItem)_playlista.ItemContainerGenerator.ContainerFromIndex(i);

                if (lvi != null)
                    asd = FindByName("PlaylistStackPanel", lvi) as StackPanel;
                else
                    asd = null;

                if (asd != null)
                    asd.Width = SzerokoscInfo.ActualWidth;
            }

            timer.Stop();
        }

        private FrameworkElement FindByName(string name, FrameworkElement root)
        {
            Stack<FrameworkElement> tree = new Stack<FrameworkElement>();
            tree.Push(root);

            while (tree.Count > 0)
            {
                FrameworkElement current = tree.Pop();
                if (current.Name == name)
                    return current;

                int count = VisualTreeHelper.GetChildrenCount(current);
                for (int i = 0; i < count; ++i)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(current, i);
                    if (child is FrameworkElement)
                        tree.Push((FrameworkElement)child);
                }
            }

            return null;
        }

        private async void ClearListClick(object sender, RoutedEventArgs e)
        {
            if (_playlista.Items.Count == 0)
                return;

            bool pytanie = false;

            var pyt = new MessageDialog("Do you want to clear items from your playlist ?", "Question");
      
            pyt.Commands.Add(new UICommand("Yes", (comand) => { pytanie = true; }));
            pyt.Commands.Add(new UICommand("No", (comand) => { pytanie = false; }));

            pyt.DefaultCommandIndex = 0;
            pyt.CancelCommandIndex = 1;

            await pyt.ShowAsync();

            if (pytanie == true)
            {
                slider.Value = 0.0;
                StopTimer();
                media.Stop();
                media.Source = null;
                _playlista.Items.Clear();
                PlayerInfo.DataContext = null;
                PlayerInfo.DataContext = domy;
            }
        }

        private async void AddSongOrFolder(object sender, RoutedEventArgs e)
        {
            var obj = (sender as Button).DataContext as IContent;

            if (obj is File)
            {
                var ele = obj as File;
                var ite = new File(ele.music, ele.Plik, ele.Cover);
                _playlista.Items.Add(ite);

            }
            else
            {
                var l = new List<File>();
                List<string> rozsz = new List<string>()
                {
                    ".mp3",
                    ".wma",
                    ".wav",
                    ".aac"
                };

                await PoberzMuzyke((obj as Directory).Folder, l, rozsz);

                foreach (var ele in l)
                {
                    var ite = new File(ele.music, ele.Plik, ele.Cover);
                    _playlista.Items.Add(ite);
                }
            }
        }

        private async void _lok_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (App._dostep != true)
            {
                await (new Windows.UI.Popups.MessageDialog("Enable access to files and folders.", "Information")).ShowAsync();
                return;
            }

            

            var obj = e.ClickedItem as IContent;

            if (obj is Directory)
            {
                var objekt = obj as Directory;
                var zbior = await objekt.Folder.GetItemsAsync();
                var nag = new Naglowek(root, objekt.Folder);
                lokalizacja = await GenerujLok(zbior);


                var poprzedni = LOK.DataContext as Naglowek;
                nag.Prev = poprzedni;

                LOK.DataContext = null;
                LOK.DataContext = nag;

                _lok.ItemsSource = null;
                _lok.ItemsSource = lokalizacja;
            }
            else
            {
                if (aktgrany != null)
                    aktgrany.Vis = false;
                
                (obj as File).Vis = true;
                aktgrany = obj as File;

                PlayerInfo.DataContext = null;
                PlayerInfo.DataContext = obj as File;

                await ReadAudioFile((obj as File).Plik);
                button1.Style = Application.Current.Resources["PlayButtonStyle"] as Style;
            }

        }

        private async void _playlista_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (App._dostep != true)
            {
                await(new Windows.UI.Popups.MessageDialog("Enable access to files and folders.", "Information")).ShowAsync();
                return;
            }

            if (aktgrany != null)
                aktgrany.Vis = false;

            
            var obj                 =   e.ClickedItem as File;
            obj.Vis = true;
            aktgrany = obj;
            PlayerInfo.DataContext  =   null;
            PlayerInfo.DataContext  =   obj;

            obj.Vis = true;

            await ReadAudioFile(obj.Plik);
            button1.Style = Application.Current.Resources["PlayButtonStyle"] as Style;
        }

        File aktgrany;

        private void DeleteSongClick(object sender, RoutedEventArgs e)
        {
         //pomysł nadać tag do najwyzszego grida i później poszukać tego grida w listview i go usunąć
            var obj = (sender as Button).DataContext as File;
            List<File> g = new List<File>(_playlista.Items.Cast<File>());
            var ind = g.IndexOf(obj);
            _playlista.Items.RemoveAt(ind);
            StopTimer();
            media.Source = null;
            button1.Style = Application.Current.Resources["PlayButtonStyle"] as Style;
            
            slider.Value = 0.0;
            PlayerInfo.DataContext = null;
            PlayerInfo.DataContext = domy;
            
        }

        private async void SaveClick(object sender, RoutedEventArgs e)
        {
            if (_playlista.Items.Count == 0 || _playlista.Items == null)
                return;

            if (PlaylistName.Text.Length == 0)
            {
                await new MessageDialog("Name your PLAYLIST").ShowAsync();
                return;
            }

            List<PlaylistFormat> format = new List<PlaylistFormat>();


            switch (this.PlaylistaCombobox.SelectedIndex)
            {
                case 0: //.m3u
                    format.Add(PlaylistFormat.M3u);
                break;
                case 1: //.wpl
                    format.Add(PlaylistFormat.WindowsMedia);
                break;
                case 2: //.zune
                    format.Add(PlaylistFormat.Zune);
                break;
                case 3: //all
                    format.Add(PlaylistFormat.M3u);
                    format.Add(PlaylistFormat.WindowsMedia);
                    format.Add(PlaylistFormat.Zune);
                break;
                default:
                    await new MessageDialog("Select PLAYLIST type").ShowAsync();
                    return;
            }

            FolderPicker folder = new FolderPicker();
            folder.FileTypeFilter.Add("*");
            folder.SuggestedStartLocation = PickerLocationId.ComputerFolder;
            folder.ViewMode = PickerViewMode.List;

            var selfolder = await folder.PickSingleFolderAsync();

            if(selfolder == null)
                return;

            List<File> lista = new List<File>(_playlista.Items.Cast<File>());
            Playlist playlis = new Playlist();
            
            
            foreach (var item in lista)
                playlis.Files.Add(item.Plik);

            var filename = PlaylistName.Text;

            foreach (var item in format)
            {
                await playlis.SaveAsAsync(selfolder, filename, NameCollisionOption.GenerateUniqueName, item);
            }

            await new MessageDialog("Your playlist/s successfully saved.").ShowAsync();
        }

        private void PlaylistName_LostFocus(object sender, RoutedEventArgs e)
        {
            (sender as TextBox).Text = Regex.Replace(PlaylistName.Text, "[^a-zA-Z0-9_]+", "", RegexOptions.None);
        }

        private void PlaylistName_TextChanged(object sender, TextChangedEventArgs e)
        {
            (sender as TextBox).Text = Regex.Replace((sender as TextBox).Text, "[^a-zA-Z0-9_]+", "", RegexOptions.None);
            (sender as TextBox).SelectionStart = (sender as TextBox).Text.Length;
            
        }

        private void Grid_Holding(object sender, HoldingRoutedEventArgs e)
        {
            if (drugiraz == false)
            {
                var obj = (sender as Grid).DataContext as File;
                List<File> g = new List<File>(_playlista.Items.Cast<File>());
                var ind = g.IndexOf(obj);
                _playlista.Items.RemoveAt(ind);
                StopTimer();
                media.Source = null;
                button1.Style = Application.Current.Resources["PlayButtonStyle"] as Style;

                slider.Value = 0.0;
                PlayerInfo.DataContext = null;

                if (_playlista.Items.Count == 0)
                    PlayerInfo.DataContext = domy;
                drugiraz = true;
            }
            else
                drugiraz = false;
        }

        private async void NewClick(object sender, RoutedEventArgs e)
        {

            bool pytanie = false;

            var pyt = new MessageDialog("Do you want to create new playlist ?", "Question");
      
            pyt.Commands.Add(new UICommand("Yes", (comand) => { pytanie = true; }));
            pyt.Commands.Add(new UICommand("No", (comand) => { pytanie = false; }));

            pyt.DefaultCommandIndex = 0;
            pyt.CancelCommandIndex = 1;

            await pyt.ShowAsync();

            if (pytanie == true)
            {
                this._playlista.Items.Clear();
                this.PlaylistaCombobox.SelectedIndex = -1;
                this.PlaylistName.Text = "";
                slider.Value = 0.0;
                StopTimer();
                media.Stop();
                media.Source = null;

                PlayerInfo.DataContext = null;
                PlayerInfo.DataContext = domy;
            }
        }

        private async void pageRoot_Loaded(object sender, RoutedEventArgs e)
        {
            await Open1();
            await Open();
        }

        private async Task<bool> Pytanie1()
        {
            var pyt = new Windows.UI.Popups.MessageDialog("Allow this application to access files and folders ?", "Question");
            pyt.Commands.Add(new UICommand("Allow", (command) => { App._dostep = true; }));
            pyt.Commands.Add(new UICommand("Decline", (command) => { App._dostep = false; }));

            pyt.DefaultCommandIndex = 0;
            pyt.CancelCommandIndex = 1;

            await pyt.ShowAsync();

            await SaveData1();

            return true;

        }

        private async Task<bool> SaveData1()
        {
            StorageFile userdetailsfile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Dane",
                                                                CreationCollisionOption.ReplaceExisting);
            IRandomAccessStream raStream = await userdetailsfile.OpenAsync(FileAccessMode.ReadWrite);

            using (IOutputStream outStream = raStream.GetOutputStreamAt(0))
            {
                bool jakidostep = App._dostep;
                DataContractSerializer serializer = new DataContractSerializer(typeof(System.Boolean));

                serializer.WriteObject(outStream.AsStreamForWrite(), jakidostep);
                await outStream.FlushAsync();
            }
            raStream.Dispose();
            return true;
        }

        public async Task<bool> Open1()
        {
            StorageFile file = null;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync("Dane");
            }
            catch (Exception) { };

            if (file == null)
            {
                await Pytanie1();
                return false;
            }

            IRandomAccessStream inStream = await file.OpenReadAsync();
            DataContractSerializer serializer = new DataContractSerializer(typeof(System.Boolean));
            App._dostep = (bool)serializer.ReadObject(inStream.AsStreamForRead());

            inStream.Dispose();
            return true;
        }

        private async void Pytanie()
        {
            var pyt = new Windows.UI.Popups.MessageDialog("Would you like to see the tutorial ?", "Question");
            pyt.Commands.Add(new UICommand("Yes", (command) => { this.Frame.Navigate(typeof(StartPage)); }));
            pyt.Commands.Add(new UICommand("No", (command) => { }));

            await pyt.ShowAsync();

            await SaveData();

        }



        private async Task<bool> SaveData()
        {
            StorageFile userdetailsfile = await ApplicationData.Current.LocalFolder.CreateFileAsync("Tut",
                                                                CreationCollisionOption.ReplaceExisting);
            return true;
        }

        private async Task<bool> Open()
        {
            StorageFile file = null;

            try
            {
                file = await ApplicationData.Current.LocalFolder.GetFileAsync("Tut");
            }
            catch (Exception) { };

            if (file == null)
            {
                Pytanie();
                return false;
            }

            return true;
        }

    }
}
